# Avaluació m02

Els instruments d'avaluació estan formats per exercicis, proves escrites i graella d'observació de l'alumne.

- **exercicis**: Es realitzaran al llarg de cadascuna de les tres unitats formatives. Seran lliurats dins del termini fixat pel professor.
- **proves escrites**: Avaluaran els coneixements adquirits. Es realitzarà un mínim d'una prova escrita per unitat formativa. Si no s'assoleix un mínim de 5 punts, la prova restarà suspesa.
- **graella d'observacions**: S'avaluarà les capacitats de l'alumne i la seva destresa en la realització dels exercicis i tasques associades:
  - Lliurament del treball en data fixada
  - Pulcritud en la presentació del treball
  - Capacitat de resolució de problemes
  - Autonomia de l'alumne
  - Relació de l'alumne amb el grup.

S'avaluarà cadascun dels instruments d'avaluació puntuant-los entre 0 i 10 punts. Es considerarà superat si la seva nota és igual o superior a 5 punts.



## Qualificació de cadascuna de les tres unitats formatives

La nota de cada unitat formativa es ponderarà:

40% Exercicis. Caldrà que s'assoleixi una nota igual o superior a 5 punts en la mitja dels exercicis.

60% Proves escrites. Caldrà obtenir un mínim de 5 punts en la mitja de les proves escrites.

## Entregues: exercicis

- Es poden fer en català, castellà o anglès.
- Es faci en l'idioma que es faci tota l'entrega ha d'estar en el mateix idioma.
- Cal tenir cura amb les faltes d'ortografia. Teniu correctors molt bons online:
    - Català: https://www.softcatala.org/corrector/
    - Castellà: https://www.corrector-castellano.com/
    - Anglès: https://www.grammarcheck.net/editor/
- Tot exercici que no tingui respostes totes les preguntes es qualificarà amb un zero. Sempre es deixa temps a classe per fer els exercicis i preguntar dubtes.
- Tot exercici entregat fora de termini pot ser qualificat amb un zero.

## Entregues: proves

- No es pot fer servir llapis ni bolígraf vermell
- Tota prova entregada sense les dades identificatives a tots els fulls pot ser qualificada amb un zero.